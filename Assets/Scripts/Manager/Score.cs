﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Score : MonoBehaviour
{

    public int score;
    public Text scoreUI;

    private void Update()
    {
        scoreUI.text = score.ToString();

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PointBox20"))
        {
            score += 20;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("PointBox50"))
        {
            score += 25;
            Destroy(collision.gameObject);
        }

    }
}
