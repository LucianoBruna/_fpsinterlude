﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkSniperSpawns : MonoBehaviour
{
    public bool isOcupped;

    private void Start()
    {
        isOcupped = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Sniper"))
        {
            isOcupped = true;
        }
        else
        {
            isOcupped = false;
        }
    }
}
