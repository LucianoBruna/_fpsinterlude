﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class LostWinMenu : MonoBehaviour
{
    public GameObject winMenu;
    public GameObject lostMenu;
    public GameObject player;
    public GameObject waveSpawner;
    public Text scoreLose;
    public Text scoreWin;

    private void Start()
    {
        winMenu.gameObject.SetActive(false);
        lostMenu.gameObject.SetActive(false);
    }

    private void Update()
    {
        if(Time.timeScale == 1)
        {
            if (player.gameObject.GetComponent<Life>().life <= 0)
            {
                lostMenu.SetActive(true);
                scoreLose.text = "You earned: " + player.GetComponent<Score>().score;
                PauseGame();

            }
            else if (this.GetComponent<Timer>().timeValue <= 0)
            {
                player.gameObject.GetComponent<Score>().score += 100;
                winMenu.SetActive(true);
                scoreWin.text = "You earned: " + player.GetComponent<Score>().score;
                PauseGame();
            }
        }

   
    }

    void PauseGame()
    {
        Time.timeScale = 0;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void RestartLevel()
    {
        //SceneManager.LoadScene("Game");
        SceneManager.UnloadSceneAsync("Game");
        SceneManager.LoadSceneAsync("Game");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
