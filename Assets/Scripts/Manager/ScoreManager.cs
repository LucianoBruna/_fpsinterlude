﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;

public class ScoreManager : MonoBehaviour
{
    Animator coin;

    public Transform camera;
    public AnimationCurve myCurve;

    float spawnPos;

    // Start is called before the first frame update
    void Start()
    {
        spawnPos = this.transform.position.y;

        if (spawnPos > 10)
            coin = GetComponent<Animator>();

        camera = Camera.main.transform;
        
    }

    // Update is called once per frame
    void Update()
    {
        


        transform.LookAt(2 * transform.position - camera.position);
        
        if(spawnPos <= 10)
        {
            transform.position = new Vector3(transform.position.x, myCurve.Evaluate((Time.time % myCurve.length)), transform.position.z);
        }
        else
        {
            coin.SetBool("flotar", true);
        }

        Destroy(this.gameObject, 5f);
    }
}
