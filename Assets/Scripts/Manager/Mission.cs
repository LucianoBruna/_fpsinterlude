﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mission : MonoBehaviour
{
    public GameObject mission;
    public bool readyToPause;

    private void Start()
    {
        readyToPause = false;
        Time.timeScale = 0;
        mission.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        

    }

    public void StartGame()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
        StartCoroutine(missionOut());
    }

    IEnumerator missionOut()
    {
        
        yield return new WaitForSeconds(5f);
        readyToPause = true;
        mission.SetActive(false);
    }

}
