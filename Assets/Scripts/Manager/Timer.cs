﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float timeValue = 300f;

    public Text time;

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 1)
        {
            if (timeValue > 0)
                DownTimer();
            else
                timeValue = 0;

            DisplayTimer(timeValue);
        }

    }

    void DownTimer()
    {

        timeValue -= Time.deltaTime;
    }

    void DisplayTimer(float timeToDisplay)
    {

        if (timeToDisplay < 0)
        {
            timeToDisplay = 0;
        }

        float minute = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        time.text = string.Format("{0:00}:{1:00}", minute, seconds);
    }
}
