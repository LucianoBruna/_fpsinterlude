﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pauseMenu : MonoBehaviour
{
    public bool paused;
    public GameObject menu;

    private void Start()
    {
        menu.SetActive(false);
    }

    void Update()
    {
        if(this.GetComponent<Mission>().readyToPause == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                paused = !paused;
            }

            if (paused == true)
            {
                PauseGame();
            }
            else if (paused == false)
            {
                ResumeGame();
            }
        }
       
    }

    void PauseGame()
    {
        menu.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 0;
    }

    void ResumeGame()
    {
        menu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
    }
}
