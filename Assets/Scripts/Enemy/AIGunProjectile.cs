﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIGunProjectile : MonoBehaviour
{
    public bool automaticGun;

    #region WeaponVariables
    public GameObject canon;
    public GameObject bullet;
    public int actualBullet;
    public int Magazine = 15;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;
    Transform target;
    public bool shoot = false;
    public bool reload = false;
    #endregion
    float interTimeBetweenShooting;
    bool canShoot;

    private void Start()
    {
        canShoot = false;
    }

    private void Update()
    {

        if (transform.parent.GetComponent<AIEnemy>().playerInAttackRange == true)
        {
            canShoot = true;
        }

        if (canShoot == true)
        {
            target = transform.parent.GetComponent<AIEnemy>().player;
            shoot = true;

            Debug.DrawLine(transform.position, target.position, Color.red);
        }
        else
        {
            target = null;
            shoot = false;
        }

        if (actualBullet == 0)
        {
            shoot = false;
            reload = true;
        }

        ShootWeapon();
        Reloading();
    }

    private void ShootWeapon()
    {
        if (shoot == true)
        {
            interTimeBetweenShooting -= Time.deltaTime;

            if (interTimeBetweenShooting <= 0)
            {

                #region Projectile Instance
                GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, this.bullet.transform.rotation);
                Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                Destroy(currentBullet, 3f);
                #endregion


                actualBullet--;

                interTimeBetweenShooting = timeBetweenShooting;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {

                actualBullet = Magazine;
                recharge = 5f;
                reload = false;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(canon.transform.position, canon.transform.forward * 10);
    }
}
