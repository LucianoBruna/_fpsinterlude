﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperSpawner : MonoBehaviour
{
	public enum SpawnState { SPAWNING, WAITING, COUNTING };

	[System.Serializable]
	public class Wave
	{
		public string name;
		public Transform[] enemy;
		public int count;
		public float rate;
	}

	public Wave[] waves;
	private int nextWave = 0;
	public int NextWave
	{
		get { return nextWave + 1; }
	}

	public Transform[] spawnPoints;

	public float timeBetweenWaves;
	private float waveCountdown;
	public float WaveCountdown
	{
		get { return waveCountdown; }
	}

	private float searchCountdown = 1f;
	private SpawnState state = SpawnState.COUNTING;
	public SpawnState State
	{
		get { return state; }
	}

	public bool noSniperAlive;

	void Start()
	{
		if (spawnPoints.Length == 0)
		{
			Debug.LogError("No spawn points referenced.");
		}

		waveCountdown = timeBetweenWaves;
	}

	void Update()
	{

		if (Time.timeScale == 1)
		{
			if (state == SpawnState.WAITING)
			{
				if (!EnemyIsAlive())
				{
					WaveCompleted();
				}
				else
				{
					return;
				}

			}

			if (waveCountdown <= 0)
			{
				if (state != SpawnState.SPAWNING)
				{
					StartCoroutine(SpawnWave(waves[nextWave]));
				}
			}
			else
			{
				waveCountdown -= Time.deltaTime;
			}
		}

	}

	void WaveCompleted()
	{
		Debug.Log("Wave Completed!");

		state = SpawnState.COUNTING;
		waveCountdown = timeBetweenWaves;

		if (nextWave + 1 > waves.Length - 1)
		{
			nextWave = 0;
			Debug.Log("ALL WAVES COMPLETE! Looping...");
		}
		else
		{
			nextWave++;
		}
	}

	public bool EnemyIsAlive()
	{
		searchCountdown -= Time.deltaTime;
		if (searchCountdown <= 0f)
		{
			searchCountdown = 1f;
			if (GameObject.FindGameObjectWithTag("Sniper") == null)
			{
				noSniperAlive = true;
				return false;

			}
		}
		noSniperAlive = false;
		return true;
	}

	IEnumerator SpawnWave(Wave _wave)
	{
		//Debug.Log("Spawning Wave: " + _wave.name);
		state = SpawnState.SPAWNING;

		for (int i = 0; i < _wave.count; i++)
		{
			SpawnEnemy(_wave.enemy[Random.Range(0, _wave.enemy.Length)]);
			yield return new WaitForSeconds(1f / _wave.rate);

		}

		state = SpawnState.WAITING;

		yield break;
	}

	void SpawnEnemy(Transform _enemy)
	{
		//Debug.Log("Spawning Enemy: " + _enemy.name);


		Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
		//Instantiate(_enemy, _sp.position, _sp.rotation);
		if (_sp.gameObject.GetComponent<checkSniperSpawns>().isOcupped == false)
        {
			Instantiate(_enemy, _sp.position, _sp.rotation);
		}
        else if (_sp.gameObject.GetComponent<checkSniperSpawns>().isOcupped == true)
        {
			resetSpawn(waves[nextWave]);

		}

    }

	void resetSpawn(Wave _wave)
    {
		SpawnEnemy(_wave.enemy[Random.Range(0, _wave.enemy.Length)]);
	}

}
