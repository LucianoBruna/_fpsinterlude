﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIRaycast : MonoBehaviour
{
    public bool automaticGun;

    public bool shotGun;

    #region WeaponVariables
    public GameObject canon;
    //public GameObject bullet;
    public int actualBullet;
    public int Magazine = 15;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;
    Transform target;
    public bool shoot = false;
    public bool reload = false;
    public float spread;
    #endregion
    public int bulletsShots;
    public int bulletsPerTap;

    float interTimeBetweenShooting;

    public GameObject shotGunSound;

    public RaycastHit rayHit;
    public LayerMask whatIsPlayer;
    public GameObject nullHitPoint;
    public int damage;
    float range;
    bool canShoot;

    public GameObject muzzleFlash;
    public GameObject muzzlePosition;


    private void Start()
    {
        canShoot = false;
    }

    private void Update()
    {
        if(Time.timeScale == 1)
        {
            range = transform.GetComponentInParent<AIEnemy>().attackRange;

            if (transform.parent.GetComponent<AIEnemy>().executeAttackScript == true)
            {
                canShoot = true;
            }
            else if(transform.parent.GetComponent<AIEnemy>().executeAttackScript == false)
            {
                canShoot = false;
            }

            if (canShoot == true)
            {
                target = transform.parent.GetComponent<AIEnemy>().player;
                shoot = true;

                Debug.DrawLine(transform.position, target.position, Color.red);
            }
            else
            {
                target = null;
                shoot = false;
            }

            if (actualBullet == 0)
            {
                shoot = false;
                reload = true;
            }

            ShootWeapon();
            Reloading();
        }
       
    }

    private void ShootWeapon()
    {
        if (shoot == true)
        {
            float x = Random.Range(-spread, spread);
            float y = Random.Range(-spread, spread);

            //Calculate Direction with Spread
            Vector3 direction = canon.transform.forward + new Vector3(x, y, 0);

            interTimeBetweenShooting -= Time.deltaTime;

            if (interTimeBetweenShooting <= 0)
            {
                if (muzzleFlash != null)
                {

                    if (shotGun == true)
                    {
                        GameObject muzzleInstatiate = Instantiate(muzzleFlash, muzzlePosition.transform.position, muzzlePosition.transform.rotation);
                        GameObject sound = Instantiate(shotGunSound, canon.transform.position, canon.transform.rotation);
                        //muzzleInstatiate.gameObject.SetActive(false);
                        Destroy(muzzleInstatiate, 0.2f);
                        Destroy(sound, 3f);
                    }
                    else if (shotGun == false)
                    {


                        GameObject muzzleInstatiate = Instantiate(muzzleFlash, muzzlePosition.transform.position, muzzlePosition.transform.rotation);
                        Destroy(muzzleInstatiate, 0.2f);
                    }
                }


                #region RC
                if(transform.parent.GetComponent<AIEnemy>().isSniper == true)
                {
                    Vector3 directionSniper= transform.parent.GetComponent<AIEnemy>().attackPoint.transform.forward;
                    if (Physics.Raycast(canon.transform.position, directionSniper, out rayHit, int.MaxValue))
                    {
                        Debug.Log(rayHit.collider.name);

                        if (transform.parent.GetComponent<AIEnemy>().rayHit.collider.CompareTag("Player"))
                        {
                            transform.parent.GetComponent<AIEnemy>().rayHit.collider.GetComponent<Life>().TakeDamage(damage); ////SCRIPT ENEMIGO
                            Debug.Log("Player hit");
                        }

                    }
                    else
                    {
                        rayHit.point = nullHitPoint.transform.position;
                    }
                }
                else if(transform.parent.GetComponent<AIEnemy>().isSniper == false)
                {
                    if (Physics.Raycast(canon.transform.position, direction, out rayHit, range, whatIsPlayer))
                    {
                        Debug.Log(rayHit.collider.name);

                        if (rayHit.collider.CompareTag("Player"))
                        {
                            rayHit.collider.GetComponent<Life>().TakeDamage(damage); ////SCRIPT ENEMIGO
                            Debug.Log("Player hit");
                        }

                    }
                    else
                    {
                        rayHit.point = nullHitPoint.transform.position;
                    }
                }
       
                #endregion

                #region Projectile Instance
                //GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, this.canon.transform.rotation);
                //Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                //rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                //Destroy(currentBullet, 3f);
                #endregion


                if(shotGun == true)
                {
                    actualBullet -= bulletsPerTap;
                    bulletsShots += bulletsPerTap;
                }
                else
                {
                    actualBullet--;
                    bulletsShots++;
                }
                

                if (automaticGun == true)
                {
                    automatic();
                }
                else if (automaticGun == false)
                {
                    burst();
                }

                //interTimeBetweenShooting = timeBetweenShooting;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            bulletsShots = 0;
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {
                if(transform.parent.GetComponent<AIEnemy>().isSniper == true)
                {
                    recharge = 8f;
                }
                else
                {
                    if(transform.parent.GetComponent<AIEnemy>().isSniper == false && shotGun == false)
                    {
                        recharge = 1f;
                    }
                    else if(transform.parent.GetComponent<AIEnemy>().isSniper == false && shotGun == true)
                    {
                        recharge = 5f;
                    }
                }
                actualBullet = Magazine;
             
                reload = false;
            }
        }
    }

    private void burst()
    {
        if(shotGun == false)
        {
            if (bulletsShots < bulletsPerTap)
            {
                interTimeBetweenShooting = 0.05f;
            }
            else if (bulletsShots >= bulletsPerTap)
            {
                interTimeBetweenShooting = timeBetweenShooting;
                bulletsShots = 0;
            }
        }
        else
        {
            if (bulletsShots < bulletsPerTap)
            {
                interTimeBetweenShooting = 0f;
            }
            else if (bulletsShots >= bulletsPerTap)
            {
                interTimeBetweenShooting = timeBetweenShooting;
                bulletsShots = 0;
            }
        }

    }

    private void automatic()
    {
        interTimeBetweenShooting = timeBetweenShooting;
    }
}

