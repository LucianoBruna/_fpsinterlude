﻿
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;

[RequireComponent(typeof(NavMeshAgent))]
public class AIEnemy : MonoBehaviour
{
    public bool isSniper;

    public NavMeshAgent agent;

    public int scoreEnemy = 5;
    public GameObject scoreBox;

    //Animation
    Animator anim;

    public GameObject gunToSpawn;
    int spawnedGun = 0;

    public Transform player;
    public GameObject attackPoint;
    public GameObject gunSpawn;

    public LayerMask whatIsGround, whatIsPlayer;

    public float health;

    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    //Attacking
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public bool executeAttackScript;
    public int damage;
    //public GameObject projectile;

    //States
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    //For Sniper
    public RaycastHit rayHit;

    private void Start()
    {
        player = GameObject.Find("FirstPerson-AIO").transform;
        agent = GetComponent<NavMeshAgent>();
        executeAttackScript = false;
        if(isSniper == true)
        {
            transform.LookAt(player);
        }
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if(Time.timeScale == 1)
        {
            if(isSniper == false)
            {
                //Check for sight and attack range
                playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
                playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

                if (!playerInSightRange && !playerInAttackRange) Patroling();
                if (playerInSightRange && !playerInAttackRange) ChasePlayer();
                if (playerInAttackRange && playerInSightRange) AttackPlayer();
            }
            
            else if (isSniper == true)
            {
                
                rayHit = new RaycastHit();

                Ray ray = new Ray(attackPoint.transform.position, player.position - attackPoint.transform.position);

                if (Physics.Raycast(ray, out rayHit, int.MaxValue))
                {
                    Debug.DrawLine(ray.origin, rayHit.point, Color.red);
                    if (rayHit.transform == player)
                    {
                        attackPoint.transform.LookAt(player);
                        Debug.Log("Player in sniper view");
                        playerInSightRange = true;
                        playerInAttackRange = true;
                        if (playerInAttackRange && playerInSightRange) AttackPlayer();
                    }
                    else if(rayHit.transform != player)
                    {
                        Debug.Log("Player is not in sniper view");
                        playerInSightRange = false;
                        playerInAttackRange = false;
                        executeAttackScript = false;
                    }
                }
            }

          
        }


    }

    private void Patroling()
    {


        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;

        anim.SetBool("Run", false);
        anim.SetBool("Idle", true);
        anim.SetBool("Shoot", false);
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.position);
        anim.SetBool("Run", true);
        anim.SetBool("Idle", false);
        anim.SetBool("Shoot", false);
    }

    private void AttackPlayer()
    {
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);

        transform.LookAt(player);

        if (!alreadyAttacked)
        {
            anim.SetBool("Run", false);
            anim.SetBool("Idle", false);
            anim.SetBool("Shoot", true);

            executeAttackScript = true;
            
            
        }
        else
        {
            executeAttackScript = false;
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0) Invoke(nameof(DestroyEnemy), 0.5f);
    }
    private void DestroyEnemy()
    {
        int randomNum = Random.Range(1, 3);
        player.gameObject.GetComponent<Score>().score += scoreEnemy;
        if (spawnedGun == 0) 
        {
            if(gunToSpawn != null)
            {
                if (randomNum <= 1)
                    Instantiate(scoreBox, gunSpawn.transform.position, Quaternion.identity);
                else
                    Instantiate(gunToSpawn, gunSpawn.transform.position, Quaternion.identity);
            }
            else if(gunToSpawn == null)
            {
                if (randomNum <= 2)
                    Instantiate(scoreBox, gunSpawn.transform.position, Quaternion.identity);
            }
           


        } 
        spawnedGun++;
        Destroy(this.gameObject);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }


}
