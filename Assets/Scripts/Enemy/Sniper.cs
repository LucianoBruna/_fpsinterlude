﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sniper : MonoBehaviour
{
    public GameObject sniperSound;
    public Transform playerPos;
    public float yPos;
    public int damage;
    public bool IsGrounded;
    public float timer;
    public float timer2;
    public GameObject sniperSpawn;


    private void Update()
    {
        yPos = playerPos.position.y;
        //sniperAiming();
        //StartCoroutine(sniperShoot());
        if (yPos >= 38 && sniperSpawn.gameObject.GetComponent<SniperSpawner>().noSniperAlive == true)
        {
            timer2 -= Time.deltaTime;

            if (timer2 <= 0)
            {
                playerPos.gameObject.GetComponent<Life>().TakeDamage(damage);
                GameObject soundInstatiate = Instantiate(sniperSound, playerPos.position, transform.rotation);
                Destroy(soundInstatiate, 2f);
                timer2 = 8f;
            }
        }
        else
        {
            timer2 = 8f;
        }

        if (yPos >= 6 && yPos <= 20)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                playerPos.gameObject.GetComponent<Life>().TakeDamage(damage);
                GameObject soundInstatiate = Instantiate(sniperSound, new Vector3(0,0,0), transform.rotation);
                Destroy(soundInstatiate, 2f);
                timer = 6f;
            }
        }
        else
        {
            timer = 6f;
        }

    }

    void SniperShoot()
    {
        playerPos.gameObject.GetComponent<Life>().TakeDamage(damage);
        GameObject soundInstatiate = Instantiate(sniperSound, playerPos.position, transform.rotation);
        Destroy(soundInstatiate, 0.15f);
    }
}
