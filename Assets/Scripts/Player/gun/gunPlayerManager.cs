﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gunPlayerManager : MonoBehaviour
{
    public GameObject laserGun;
    bool pHGun;
    public Transform Belt;
    public Transform hand;


    private void Update()
    {
        if (GetComponent<PickUpController>().playerHaveGun == true)
            pHGun = true;

        if (pHGun == true)
            LaserGunToBelt();
        else
            LaserGunToHand();
    }

    void LaserGunToBelt()
    {
        laserGun.gameObject.transform.SetParent(Belt);
    }
    void LaserGunToHand()
    {
        laserGun.gameObject.transform.SetParent(hand);
    }

}
