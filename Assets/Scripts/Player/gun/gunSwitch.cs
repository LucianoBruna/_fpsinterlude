﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class gunSwitch : MonoBehaviour
{
    public int gunsEquipped;
    public bool not2ndGun;
    //public GameObject laserGun;
    //public GameObject bHGun;

    public int armaEnMano = 0;

    void Start()
    {
        gunsEquipped = 0;
        not2ndGun = true;
        //laserGun.SetActive(true);
        armaActual();
    }


    void Update()
    {
  

        if (Time.timeScale == 1)
        {
            int armaAnterior = armaEnMano;

            //if (armaEnMano == 0)
            //{
            //    laserGun.SetActive(true);
            //    bHGun.SetActive(false);
            //}
            //else if (armaEnMano == 1)
            //{
            //    bHGun.SetActive(true);
            //    laserGun.SetActive(false);
            //}
            //if (Input.GetKeyDown(KeyCode.Q) && armaEnMano == 0)
            //{
            //    armaEnMano = 1;
            //}
            //else if (Input.GetKeyDown(KeyCode.Q) && armaEnMano == 1)
            //{
            //    armaEnMano = 0;
            //}

            if (not2ndGun == true)
            {
                armaEnMano = 0;
            }
            else
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0f)
                {
                    if (armaEnMano >= transform.childCount - 1) armaEnMano = 0;

                    else armaEnMano++;
                }

                if (Input.GetAxis("Mouse ScrollWheel") < 0f)
                {
                    if (armaEnMano <= 0) armaEnMano = transform.childCount - 1;
                    else armaEnMano--;
                }

            }



            if (armaAnterior != armaEnMano)
            {
                armaActual();
            }

        }


    }
    void armaActual()
    {

        int i = 0;
        foreach (Transform arma in transform)
        {
            if (i == armaEnMano)
                arma.gameObject.SetActive(true);
            else
                arma.gameObject.SetActive(false);
            i++;
        }
    }


}
