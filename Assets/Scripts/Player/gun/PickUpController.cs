﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpController : MonoBehaviour
{
    public int gunsEquipped;

    public gunRaycast gunScript;
    public Rigidbody rb;
    GameObject playerR, gunContR, fpsCamR, headR;
    public BoxCollider coll;
    public Transform player, gunContainer, fpsCam, head;
    Vector3 distanceToPlayer;
    public float pickUpRange;
    public float dropForwardForce, dropUpwardForce;
    public bool equipped;
    public bool slotFull;
    public float timer;
    public float timeToDestroy = 6;
    public GameObject target;
    public bool playerHaveGun;

    private void Start()
    {
        gunsEquipped = 0;
        slotFull = false;
        equipped = false;
        playerHaveGun = false;
        target.SetActive(true);
        timer = 0f;

        this.playerR = GameObject.Find("FirstPerson-AIO");
        this.gunContR = GameObject.Find("Hand");
        this.fpsCamR = GameObject.Find("Player Camera");
        headR = GameObject.Find("HeadJoint");

        this.player = playerR.transform;
        this.gunContainer = gunContR.transform;
        this.fpsCam = fpsCamR.transform;
        head = headR.transform;

        //Setup
        if (!equipped)
        {
            gunScript.enabled = false;
            rb.isKinematic = false;
            coll.isTrigger = false;
        }
        if (equipped)
        {
            gunScript.enabled = true;
            rb.isKinematic = true;
            coll.isTrigger = true;
            slotFull = true;
        }
    }

    private void Update()
    {

        //if(playerR.GetComponent<gunSwitch>().transform.childCount > 0)
        //{
        //    equipped = true;
        //    slotFull = true;
        //    playerHaveGun = true;
        //}
        //else
        //{
        //    equipped = false;
        //    slotFull = false;
        //    playerHaveGun = false;
        //}

        if (!equipped)
            destroy();
        else if(equipped)
            timeToDestroy = 6f;

        //Check if player is in range and "E" is pressed
        distanceToPlayer = player.position - transform.position;
        //if (!equipped && distanceToPlayer.magnitude <= pickUpRange && Input.GetKeyDown(KeyCode.E) && !slotFull) PickUp();

        //Drop if equipped and "Q" is pressed
        if (equipped && Input.GetKeyDown(KeyCode.Q)) 
        {
            
            Drop();
            
        }
        if (equipped && this.gameObject.GetComponent<gunRaycast>().bulletsLeft == 0 && this.gameObject.GetComponent<gunRaycast>().magazineSize == 0)
        {
            Drop();
        }

        if (timer >= 0)
        {
            timer -= Time.deltaTime;
        }
        if (!equipped && timer <= 0f)
        {
            //rb.isKinematic = false;
            coll.isTrigger = false;
            //timer = 1f;
        }


    }


    private void OnCollisionEnter(Collision collision)
    {
        if (gunContainer.GetComponent<gunSwitch>().gunsEquipped == 0 && playerHaveGun == false && !equipped && distanceToPlayer.magnitude <= pickUpRange && collision.gameObject.CompareTag("Player") && !slotFull) 
        {
            PickUp();
        }
        
    }

    private void PickUp()
    {
        gunContainer.GetComponent<gunSwitch>().gunsEquipped = 1;
        this.gameObject.SetActive(false);
        equipped = true;
        slotFull = true;
        playerHaveGun = true;
        gunContainer.gameObject.GetComponent<gunSwitch>().not2ndGun = false;
        //Make weapon a child of the camera and move it to default position
        transform.SetParent(gunContainer);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(Vector3.zero);
        transform.localScale = Vector3.one;

        target.SetActive(false);

        //Make Rigidbody kinematic and BoxCollider a trigger
        rb.isKinematic = true;
        coll.isTrigger = true;

        //Enable script
        gunScript.enabled = true;
    }

    private void Drop()
    {
        gunContainer.GetComponent<gunSwitch>().gunsEquipped = 0;
        equipped = false;
        slotFull = false;
        playerHaveGun = false;
        gunContainer.gameObject.GetComponent<gunSwitch>().not2ndGun = true;
        //Set parent to null
        transform.SetParent(null);

        //Make Rigidbody not kinematic and BoxCollider normal
        rb.isKinematic = false;
        //coll.isTrigger = false;

        //Gun carries momentum of player
        rb.velocity = player.GetComponent<Rigidbody>().velocity;

        target.SetActive(true);

        //AddForce
        rb.AddForce(head.forward * dropForwardForce, ForceMode.Impulse);
        rb.AddForce(head.up * dropUpwardForce, ForceMode.Impulse);
        //Add random rotation
        float random = Random.Range(-1f, 1f);
        rb.AddTorque(new Vector3(random, random, random) * 10);


        //Disable script
        gunScript.enabled = false;

        timer = 0.4f;

    }

    private void destroy()
    {
        timeToDestroy -= Time.deltaTime;

        if (timeToDestroy <= 0 || this.transform.position.y < -1)
        {
            Destroy(this.gameObject);
        }
        
    }
}
