﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoUI : MonoBehaviour
{
    public GameObject gunAmmo;
    public GameObject laserAmmo;
    public GameObject player;


    private void Update()
    {
        if(player.gameObject.GetComponent<gunSwitch>().armaEnMano != 0)
        {
            laserAmmo.gameObject.SetActive(false);
            gunAmmo.gameObject.SetActive(true);
        }

        else
        {
            gunAmmo.gameObject.SetActive(false);
            laserAmmo.gameObject.SetActive(true);
        }
            
    }
}
