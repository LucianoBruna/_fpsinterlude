﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(SphereCollider))]
public class attractP : MonoBehaviour
{
    public float ForceSpeed;

    public float attractionRange;
    public float stopAttractionRange;
    public LayerMask m_Layers;
    public int damage;
    public float maxLifetime;
    public float damageTimer;
    public float interDamageTimer;
    public Collider[] colliders;

    

    private void Update()
    {
        AttractEffect();
        interDamageTimer = damageTimer;
        //Count down lifetime
        maxLifetime -= Time.deltaTime;
        if (maxLifetime <= 0) DestroyBH();

    }

    void AttractEffect()
    {
        colliders = Physics.OverlapSphere(this.transform.position, attractionRange, m_Layers);


        foreach (var collider in colliders)
        {
            //PullOBJ = collider.gameObject;

            float dist = Vector3.Distance(collider.transform.position, this.transform.position);

            //if (dist <= attractionRange)
            //{
            //    collider.transform.position = Vector3.MoveTowards(collider.transform.position, transform.position, ForceSpeed * Time.deltaTime);

            //    if (collider.gameObject.CompareTag("Enemy"))
            //    {
            //        collider.GetComponent<AIEnemy>().TakeDamage(damage);
            //    }

            //}

            //ENEMY SCRIPT
            if (dist <= stopAttractionRange && collider.gameObject.GetComponent<AIEnemy>() != null)
            {
                collider.transform.position = Vector3.MoveTowards(collider.transform.position, transform.position, ForceSpeed * Time.deltaTime);
                interDamageTimer -= Time.deltaTime;
                if (interDamageTimer <= 0)
                {
                    collider.GetComponent<AIEnemy>().TakeDamage(damage);
                    interDamageTimer = damageTimer;
                }


            }



        }
    }

    private void Damage()
    {
        ////ENEMY SCRIPT
        //GetComponent<EnemyIA>().TakeDamage(damage);
    }

    private void DestroyBH()
    {

        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attractionRange);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, stopAttractionRange);
    }

}
