﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EZCameraShake;

public class gunRaycast : MonoBehaviour
{
    #region Gun stats
    public int damage;
    public float timeBetweenShooting, spread, range, reloadTime, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    public bool allowButtonHold;
    public int bulletsLeft, bulletsShot;
    #endregion

    #region bools
    bool shooting, readyToShoot, reloading;
    public bool laserGun;
    #endregion

    #region Reference
    public bool shotGun;
    public bool ak;
    public Camera fpsCam;
    public Transform attackPoint;
    public RaycastHit rayHit;
    public LayerMask whatIsEnemy;
    public GameObject nullHitPoint;
    #endregion

    public GameObject reloadSound;

    #region Graphics
    public GameObject bulletHoleGraphic;
    public GameObject muzzleFlash;
    GameObject muzzleInstatiate;
    public GameObject soundShot;
    public GameObject shotGunSound;
    public GameObject laserSound;
    //public bool muzzle = false;
    //public float timer = 0.1f;
    //public CamShake camShake; ////QUIERO QUE LA CAMARA TIEMBLE AL DISPARAR??
    //public float camShakeMagnitude, camShakeDuration;
    public Text ammo;
    public Text magazine;
    public GameObject Laser;

    //LineRenderer laserLineRenderer;
    #endregion

    private void Awake()
    {
        bulletsLeft = magazineSize;
        readyToShoot = true;
        fpsCam = Camera.main;
    }

    private void Start()
    {
        //ammo.text = bulletsLeft.ToString();
        muzzleFlash.SetActive(false);
        fpsCam = Camera.main;

        if (laserGun == true)
        {
            //laserLineRenderer = fpsCam.GetComponent<LineRenderer>();
            //laserLineRenderer.startWidth = 0.2f;
            //laserLineRenderer.endWidth = 0.05f;
            //laserLineRenderer.SetPosition(0, attackPoint.transform.position);
            //laserLineRenderer.enabled = false;
            Laser.SetActive(false);
        }
     

    }

    private void Update()
    {
        //Laser.transform.position = attackPoint.transform.position;
        //Laser.transform.rotation = fpsCam.transform.rotation;

        


        fpsCam = Camera.main;
        if (Time.timeScale == 1)
        {
            MyInput();

            //if (muzzle == true && laserGun != true)
            //    muzzleFlash.gameObject.SetActive(true);
            //else if (muzzle == false && laserGun != true)
            //    muzzleFlash.gameObject.SetActive(false);

            //if (timer <= 0) 
            //{
            //    muzzle = false;

            //}


            if (laserGun == true)
            {
                //laserLineRenderer.SetPosition(0, attackPoint.transform.position);
                ////ammo.text = bulletsLeft.ToString();
            }
                

            //SetText
            else if (laserGun == false)
            {
                ammo = GameObject.FindGameObjectWithTag("ammoGun").GetComponent<Text>();
                //ammo.text = bulletsLeft.ToString();
                magazine = GameObject.FindGameObjectWithTag("magazineGun").GetComponent<Text>();
                magazine.text = magazineSize.ToString();
            }

            ammo.text = bulletsLeft.ToString();

        }

    }

    private void MyInput()
    {
        if (allowButtonHold) shooting = Input.GetKey(KeyCode.Mouse0);
        else shooting = Input.GetKeyDown(KeyCode.Mouse0);

        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading) Reload();

        //Shoot
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            bulletsShot = bulletsPerTap;
            Shoot();
        }

    }

    private void Shoot()
    {
        readyToShoot = false;

        //Spread
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        //Calculate Direction with Spread
        Vector3 direction = fpsCam.transform.forward + new Vector3(x, y, 0);

        if(laserGun == true)
        {
            //laserLineRenderer.SetPosition(0, attackPoint.transform.position);
        }

        else if (laserGun == false)
        {
            //timer = 0.1f;
            //Graphics
            GameObject holeInstantiate = Instantiate(bulletHoleGraphic, rayHit.point, Quaternion.Euler(0, 180, 0));
            Destroy(holeInstantiate, 1f);

            //muzzleInstatiate = Instantiate(muzzleFlash, attackPoint.position, fpsCam.transform.rotation);
            //if(muzzleInstatiate != false)
            //    Destroy(muzzleInstatiate, 0.1f);
            
            if (soundShot != null) 
            {
                if(shotGun == true)
                {
                    muzzleFlash.SetActive(true);
                    StartCoroutine(wait(0.2f));
                    GameObject soundInstatiate = Instantiate(soundShot, attackPoint.position, fpsCam.transform.rotation);
                    Destroy(soundInstatiate, 1f);
                }
                else if (shotGun == false)
                {
                    
                    if(ak == true)
                    {
                        //StartCoroutine(wait(0.01f));
                        GameObject soundInstatiate = Instantiate(soundShot, attackPoint.position, fpsCam.transform.rotation);
                        if(soundInstatiate != null)
                        {
                            muzzleFlash.SetActive(true);
                            
                        }
                        Destroy(soundInstatiate, 0.15f);
                        StartCoroutine(wait(0.1f));

                    }
                    else
                    {
                        muzzleFlash.SetActive(true);
                        StartCoroutine(wait(0.1f));
                        GameObject soundInstatiate = Instantiate(soundShot, attackPoint.position, fpsCam.transform.rotation);
                        Destroy(soundInstatiate, 0.15f);
                    }
                    

                }
            }

            CameraShaker.Instance.ShakeOnce(1f, 1f, 1f, 1f);

            //if (timer <= 0) muzzle = false;

        }
    
        //RayCast
        if (Physics.Raycast(fpsCam.transform.position, direction, out rayHit, range, whatIsEnemy))
        {
            //Debug.Log(rayHit.collider.name);

            if (rayHit.collider.CompareTag("Enemy") || rayHit.collider.CompareTag("Sniper"))
                rayHit.collider.GetComponent<AIEnemy>().TakeDamage(damage); ////SCRIPT ENEMIGO
        }
        else
        {
            rayHit.point = nullHitPoint.transform.position;
        }

        //ShakeCamera
        //camShake.Shake(camShakeDuration, camShakeMagnitude);

  
        //Instantiate(bulletHoleGraphic, rayHit.point, Quaternion.Euler(0, 180, 0));
        //Instantiate(muzzleFlash, attackPoint.position, Quaternion.identity);

        //laserLineRenderer.SetPosition(0, attackPoint.transform.position);

        if (laserGun == true)
        {
            //laserLineRenderer.SetPosition(1, rayHit.point);
            Laser.SetActive(true);
            GameObject sound = Instantiate(laserSound, attackPoint.position, fpsCam.transform.rotation);
            Destroy(sound, 0.8f);
            //laserLineRenderer.enabled = true;
            CameraShaker.Instance.ShakeOnce(.8f, 0.5f, 1f, 1f);
        }
        

        //bulletsLeft -= bulletsPerTap;
        //bulletsShot -= bulletsPerTap;

        if (shotGun == true)
        {
            bulletsLeft--;
            bulletsShot--;
            ammo.text = bulletsLeft.ToString();
        }
        else if(shotGun == false)
        {
            bulletsLeft--;
            bulletsShot--;
        }

        StartCoroutine(laserRetardo());
        Invoke("ResetShot", timeBetweenShooting);

        if (bulletsShot > 0 && bulletsLeft > 0)
            Invoke("Shoot", timeBetweenShots);
    }

    private void ResetShot()
    {
        readyToShoot = true;
    }

    IEnumerator wait(float time)
    {
        yield return new WaitForSeconds(time);
        muzzleFlash.SetActive(false);
    }
    IEnumerator laserRetardo()
    {
        yield return new WaitForSeconds(0.1f);
        if(laserGun == true)
        {
            //laserLineRenderer.enabled = false;
            Laser.SetActive(false);
        }
        
    }

    private void Reload()
    {
        reloading = true;
        GameObject reloadingSound = Instantiate(reloadSound, attackPoint.position, fpsCam.transform.rotation);
        Destroy(reloadingSound, 2f);
        Invoke("ReloadFinished", reloadTime);
    }
    private void ReloadFinished()
    {
        bulletsLeft = magazineSize;
        if(laserGun != true)
            magazineSize = 0;
        reloading = false;
    }

}
