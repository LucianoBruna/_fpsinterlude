﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class JetPack : MonoBehaviour
{
    public bool canJet; //The player can use the jetpack or not.
    public float jetForce; //Power of the jetpack.
    public float jetWait; //How long it'll take to the jetpack to start that recover the fuel.
    public float jetRecovery; // How quickly the fuel recovery.
    public float jetMaxFuel; //How much fuel the jetpack have.
    public GameObject jetSmoke;
    public GameObject fallEffect;
    public GameObject sound;
    GameObject Sound;
    public bool didJump;
    Rigidbody rb;
    public bool IsGrounded;
    public float jetCurrentFuel; //The current jetpack fuel.
    public float jetCurrentRecovery;
    int instanceTimes = 0;

    public bool audioExist;

    private void Start()
    {
        jetSmoke.SetActive(false);
        audioExist = false;
        jetCurrentRecovery = jetMaxFuel;
        rb = GetComponent<FirstPersonAIO>().fps_Rigidbody;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space) && IsGrounded == false)
        {
            didJump = true;
        }
        IsGrounded = GetComponent<FirstPersonAIO>().IsGrounded;
        if (didJump == true)
        {
            jetCurrentRecovery = 0f;
            instanceTimes = 0;
        }
        if (IsGrounded) didJump = false;

        if(didJump == true && Input.GetKey(KeyCode.Space) && audioExist == false && jetCurrentFuel > 0 && IsGrounded == false)
        {
            jetSmoke.SetActive(true);
            Sound = Instantiate(sound, rb.position, rb.transform.rotation);
            Sound.transform.SetParent(this.gameObject.transform);
            audioExist = true;

        }
        else if(didJump == false || !Input.GetKey(KeyCode.Space) || jetCurrentFuel <= 0)
        {
            if(audioExist == true)
            {
                audioExist = false;
                Destroy(Sound);
            }
            jetSmoke.SetActive(false);
           
        }
    }

    private void FixedUpdate()
    {
        bool jet = Input.GetKey(KeyCode.Space);



        #region Jet
        if (didJump == true && !IsGrounded)
        {
            canJet = true;
        }
        if(IsGrounded == true)
        {
            canJet = false;
        }
        if(canJet == true && jet == true && jetCurrentFuel > 0)
        {
            rb.AddForce(Vector3.up * jetForce * Time.fixedDeltaTime, ForceMode.Acceleration);
            jetCurrentFuel = Mathf.Max(0, jetCurrentFuel - Time.fixedDeltaTime);
        }
        if(IsGrounded == true)
        {
            
            if (jetCurrentRecovery < jetWait)
            {
                jetCurrentRecovery = Mathf.Min(jetWait, jetCurrentRecovery + Time.fixedDeltaTime);

                
                instanceTimes += 1;

                if (instanceTimes == 1)
                {
                    GameObject fallSmoke = Instantiate(fallEffect, jetSmoke.transform.position, transform.rotation);
                    Destroy(fallSmoke, 0.5f);
                }

                
            }


            else 
            {
                jetCurrentFuel = Mathf.Min(jetMaxFuel, jetCurrentFuel + Time.fixedDeltaTime * jetRecovery);
                //Debug.Log("El else");
            }
        }

        #endregion
    }
}
