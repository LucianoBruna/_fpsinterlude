﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EZCameraShake;

public class Life : MonoBehaviour
{
    public int life;
    public Slider lifeSlider;
    public float restartLife;
    public int damageGet;

    private void Start()
    {
        restartLife = 23f;
        lifeSlider.maxValue = life;
    }

    private void Update()
    {
        lifeSlider.value = life;

        

        

        if(restartLife <= 0) 
        {
            //for(int i = damageGet; i < 1000; i++)
            //{
            //    life += 1;
            //}
            if(life < 1000) life += 100;


            if (life >= 1000)
            {
                life = 1000;
                restartLife = 23f;
            }
            
        }

        if(restartLife > 0 && life < 1000)
            restartLife -= Time.deltaTime;
        else
        {
            restartLife = 23f;
        }

    }

    public void TakeDamage(int damage)
    {
        CameraShaker.Instance.ShakeOnce(2f, 0.5f, 0.5f, 1f);
        life -= damage;
        damageGet = 1000 - life;
        restartLife = 23f;
    }
}
